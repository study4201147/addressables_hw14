using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace SampleGame
{
    public sealed class PauseScreen : MonoBehaviour
    {
        [SerializeField]
        private Button resumeButton;

        [SerializeField]
        private Button exitButton;

        private MenuLoader menuLoader;

        [Inject]
        public void Construct(MenuLoader menuLoader)
        {
            this.menuLoader = menuLoader;
            this.gameObject.SetActive(false);
            AddListeners();
            Time.timeScale = 1; //KISS
        }

        private void OnEnable()
        {
            AddListeners();
        }

        private void OnDisable()
        {
            this.resumeButton.onClick.RemoveListener(this.Hide);
            if (menuLoader != null)
            {
                this.exitButton.onClick.RemoveListener(this.menuLoader.LoadMenu);
            }
        }

        public void Show()
        {
            Time.timeScale = 0; //KISS
            this.gameObject.SetActive(true);
        }

        public void Hide()
        {
            Time.timeScale = 1; //KISS
            this.gameObject.SetActive(false);
        }

        private void AddListeners()
        {
            this.resumeButton.onClick.AddListener(this.Hide);
            if (menuLoader != null)
            {
                this.exitButton.onClick.AddListener(this.menuLoader.LoadMenu);
            }
        }
    }
}