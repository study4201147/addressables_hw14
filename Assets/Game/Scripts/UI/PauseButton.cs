using UnityEngine;
using UnityEngine.UI;

namespace SampleGame
{
    public sealed class PauseButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;
        
        [SerializeField]
        private PrefabPauseLoader pauseScreenLoader;

        private PauseScreen pauseScreen;
        
        private void OnEnable()
        {
            pauseScreenLoader.OnCompleted += OnLoaded;
        }

        private void OnDisable()
        {
            pauseScreenLoader.OnCompleted -= OnLoaded;
            if (pauseScreen != null)
            {
                this.button.onClick.RemoveListener(this.pauseScreen.Show);
            }
        }

        private void OnLoaded(PauseScreen pauseScreen)
        {
            this.pauseScreen = pauseScreen;
            this.button.onClick.AddListener(this.pauseScreen.Show);
        }
    }
}