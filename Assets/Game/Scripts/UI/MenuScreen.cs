using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace SampleGame
{
    public sealed class MenuScreen : MonoBehaviour
    {
        [SerializeField]
        private Button startButton;

        [SerializeField]
        private Button exitButton;
        
        private ApplicationExiter applicationExiter;
        private GameLoader gameLoader;
        
        [Inject]
        public void Construct(ApplicationExiter applicationFinisher, GameLoader gameLoader)
        {
            this.gameLoader = gameLoader;
            this.applicationExiter = applicationFinisher;
            AddListeners();
        }

        private void OnEnable()
        {
            AddListeners();
        }

        private void OnDisable()
        {
            if (gameLoader != null)
            {
                this.startButton.onClick.RemoveListener(this.gameLoader.LoadGame);
            }

            if (applicationExiter != null)
            {
                this.exitButton.onClick.RemoveListener(this.applicationExiter.ExitApp);
            }
        }

        private void AddListeners()
        {
            if (gameLoader != null)
            {
                this.startButton.onClick.AddListener(this.gameLoader.LoadGame);
            }

            if (applicationExiter != null)
            {
                this.exitButton.onClick.AddListener(this.applicationExiter.ExitApp);
            }
        }
    }
}