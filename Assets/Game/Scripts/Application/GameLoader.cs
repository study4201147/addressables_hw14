using UnityEngine.AddressableAssets;

namespace SampleGame
{
    public sealed class GameLoader
    {
        public void LoadGame()
        {
            Addressables.LoadSceneAsync("Game");
        }
    }
}