using UnityEngine;
using Zenject;

namespace SampleGame
{
    public sealed class MenuInstaller : MonoInstaller
    {
        [SerializeField] private PrefabMenuLoader PrefabMenuLoader;

        public override void InstallBindings()
        {
            this.Container
                .BindInstance(PrefabMenuLoader);
        }
    }
}