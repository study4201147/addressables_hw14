using UnityEngine;
using Zenject;

namespace SampleGame
{
    public sealed class PauseInstaller : MonoInstaller
    {
        [SerializeField] 
        private PrefabPauseLoader PrefabPauseLoader;

        [SerializeField] 
        private PauseButton PauseButton;

        public override void InstallBindings()
        {
            this.Container
                .BindInstance(PrefabPauseLoader);
            
            this.Container
                .BindInstance(PauseButton);
        }
        
    }
}