using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace SampleGame
{
    public sealed class LocationLoader : MonoBehaviour
    {
        [SerializeField]
        private AssetReference Location;

        private AsyncOperationHandle<GameObject> handle;

        private void OnTriggerEnter(Collider other)
        {
            handle = Addressables.InstantiateAsync(Location);
        }

        private void OnDestroy()
        {
            if (handle.IsValid())
            {
                Addressables.ReleaseInstance(handle);
            }
        }
    }
}