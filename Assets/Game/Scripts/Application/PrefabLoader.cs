using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Zenject;

namespace SampleGame
{
    public class PrefabLoader<T> : MonoBehaviour 
        where T : Component
    {
        public event Action<T> OnCompleted;
        
        [SerializeField]
        private AssetReference AssetReference;

        private AsyncOperationHandle<GameObject> handle;
        private DiContainer container;
        private T cachedScreen;
        

        [Inject]
        private void Construct(DiContainer diContainer)
        {
            container = diContainer;
        }
        
        private async void Start()
        {
            handle = Addressables.LoadAssetAsync<GameObject>(AssetReference);

            await handle.Task;

            cachedScreen = Instantiate(handle.Result, transform).GetComponent<T>();
            container.Inject(cachedScreen);
            
            OnCompleted?.Invoke(cachedScreen);
        }

        private void OnDestroy()
        {
            Addressables.ReleaseInstance(cachedScreen.gameObject);
        }
    }
}